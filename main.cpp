#include "headers/game.hpp"

float Player::v_ = 3;

int main()
{
sf::RenderWindow window(sf::VideoMode(800, 600), "SFML");
window.setFramerateLimit(60);
window.setVerticalSyncEnabled(true);

Game gm(800, 600);
while (window.isOpen())
    {
    sf::Event event;
    while (window.pollEvent(event))
        {
        if (event.type == sf::Event::Closed)
            window.close();
        if (event.type == sf::Event::KeyPressed)
            {
            gm.KeyPress(event.key.code);
            }
        if (event.type == sf::Event::KeyReleased)
            {
            gm.KeyRelease(event.key.code);
            }
        if (event.type == sf::Event::MouseButtonPressed)
            {
            /// TODO
            }
        if (event.type == sf::Event::MouseButtonReleased)
            {
            /// TODO
            }
        }
    gm.Update();
    window.clear(sf::Color(255, 255, 255, 255));
    gm.Draw(window);
    window.display();
    }
return 0;
}
