#include <SFML/Graphics.hpp>
#include <iostream>
#include <cstring>
#include <cmath>

#include "classes.hpp"
#include "gameobject.hpp"
#include "models.hpp"
#include "player.hpp"
#include "monster.hpp"


const char* TEXTURES_FOLDER = "sprites/";

const char* TEXTURE_FILES[] =
    {
    "notfound.png",
    "headfront.png",
    "headright.png",
    "headback.png",
    "bodyfront.png",
    "bodyback.png",
    "lefthand.png",
    "righthand.png",
    "leftleg.png",
    "rightleg.png",
    "creep1.png",
    "creep2.png",
    "bodyright.png",
    "healthbar.png",
    "healthfull.png",
    "creephealth.png",
    "legup.png",
    "legdown.png"};

const int MAX_TEXTURES = sizeof(TEXTURE_FILES)/sizeof(TEXTURE_FILES[0]);



Game::Game(int scrw, int scrh):scrw_(scrw),scrh_(scrh)
        {
        textures_ = new sf::Texture [MAX_TEXTURES];
        char filepath[1024] = {0};
        for (unsigned int i = 0; i < MAX_TEXTURES; i++)
            {
            strcpy(filepath, TEXTURES_FOLDER);
            strcat(filepath, TEXTURE_FILES[i]);
            if (!textures_[i].loadFromFile(filepath))
                {
                std::cout << "File \"" << TEXTURE_FILES[i] << "\" not found" << std::endl;
                return;
                }
            }
        model = new GameModel(*this);
        player_ = new Player(*this, scrw_/2, scrh_/2);
        creep_ = new Creep(*this);
        healthbar_.setTexture(GetTexture("healthbar.png"));
        healthfull_.setTexture(GetTexture("healthfull.png"));
        healthbar_.setPosition(20, 20);
        healthfull_.setPosition(20, 20);
        sf::Rect<int> hprect = healthfull_.getTextureRect();
        healthwidth_ = hprect.width;
        healthheight_ = hprect.height;
        }
sf::Texture& Game::GetTexture(char* filename)
    {
    for (int i = 0; i < MAX_TEXTURES; i++)
        {
        if (strcmp(TEXTURE_FILES[i], filename) == 0) { return textures_[i]; }
        }
    return textures_[0];
    }
Game::~Game()
        {
        delete creep_;
        delete player_;
        delete[] textures_;
        }
int Game::Update()
    {
    creep_->Update();
    player_->Update();
    model->Update(player_->GetXPos()+200, 200);
    if (player_->GetHP() <= 0)
        {
        delete player_;
        player_ = new Player(*this, scrw_/2, scrh_/2);
        }
    float hpdelta = player_->GetRelativeHP();
    healthfull_.setTextureRect(sf::Rect<int>(0,0, healthwidth_*hpdelta, healthheight_));
    return 0;
    }
int Game::Draw(sf::RenderWindow& window)
    {
    //model->RotateRel(0.1f);
    //std::cout << "Draw all scene..." << std::endl;
    if (player_->GetYPos() < creep_->GetYPos())
        {
        player_->Draw(window);
        creep_->Draw(window);
        }
    else
        {
        creep_->Draw(window);
        player_->Draw(window);
        }
    model->Draw(window);
    window.draw(healthfull_);
    window.draw(healthbar_);
    return 0;
    }
float Game::GetDistanceToCreep()
    {
    sf::Vector2f p = GetPlayerPos();
    float cy = creep_->GetYPos();
    float cx = creep_->GetXPos();
    return sqrt((cy-p.y)*(cy-p.y)+(cx-p.x)*(cx-p.x));
    }
sf::Vector2f Game::GetVectorToCreep()
    {
    sf::Vector2f p = GetPlayerPos();
    float cy = creep_->GetYPos();
    float cx = creep_->GetXPos();
    float dst = GetDistanceToCreep();
    return sf::Vector2f((cx-p.x)/dst, (cy-p.y)/dst);
    }
float Game::GetDistanceToPlayer(float ax, float ay)
    {
    return player_->GetDist(ax, ay);
    }
int Game::KickPlayer(int damage)
    {
    player_->Kick(damage);
    model->Mirror();
    return 0;
    }
int Game::Drop (float x, float y, int itemid)
    {
    ;
    }
sf::Vector2f Game::GetPlayerPos()
    {
    return player_->GetPos();
    }
void Game::MouseClick (int x, int y)
    {
    ;
    }
void Game::KeyPress(int code)
    {
    std::cout << "key " << code << " pressed" << std::endl;
    switch (code)
        {
        case 'W'-'A':
            player_->GoUp();
            break;
        case 'S'-'A':
            player_->GoDown();
            break;
        case 'A'-'A':
            player_->GoLeft();
            break;
        case 'D'-'A':
            player_->GoRight();
            break;
        default: {;}
        };
    }
void Game::KeyRelease(int code)
    {
    std::cout << "key " << code << " released" << std::endl;
    switch (code)
        {
        case 'W'-'A':
            player_->StopUp();
            break;
        case 'S'-'A':
            player_->StopDown();
            break;
        case 'A'-'A':
            player_->StopLeft();
            break;
        case 'D'-'A':
            player_->StopRight();
            break;
        default: {;}
        };
    }
