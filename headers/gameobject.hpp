

class GameObject
    {
    public:
    GameObject(Game& gm):gm_(gm)
        {
        ;
        }
    virtual ~GameObject() {;}
    virtual void Draw() {;}
    virtual void Update() {;}
    virtual float GetXPos() { return x_; }
    virtual float GetYPos() { return y_; }

    sf::Vector2f GetPos() { return sf::Vector2f(x_,y_); }
    void SetPos (float x, float y)
        {
        x_ = x;
        y_ = y;
        }
    float GetDistanceTo(GameObject* obj)
        {
        sf::Vector2f dest = obj->GetPos();
        return sqrt((x_-dest.x)*(x_-dest.x)+(y_-dest.y)*(y_-dest.y));
        }
    sf::Vector2f GetDirectionTo(GameObject* obj)
        {
        sf::Vector2f dest = obj->GetPos();
        float dist = GetDistanceTo(obj);
        return sf::Vector2f ((dest.x-x_)/dist, (dest.y-y_)/dist);
        }
    protected:
    Game& gm_;
    float x_, y_;

    };

class GamePers: public GameObject
    {
    public:
    GamePers(Game& gm):GameObject(gm)
        {
        ;
        }
    virtual ~GamePers() {;}
    virtual void Draw() {;}
    virtual void Update() { x_+=vx_; y_+=vy_; }
    float GetHP() { return hp_; }
    float GetMaxHP() { return max_hp_; }
    float GetRelativeHP() { return hp_/max_hp_; }
    void Heal(float deltahp)
        {
        if (deltahp < 0) { std::cout << "Heal value is negative\n"; }
        hp_ += deltahp;
        if (hp_ < max_hp_) { hp_ = max_hp_; }
        }
    void Kick(float damage)
        {
        if (damage < 0) { std::cout << "Damage value is negative\n"; }
        hp_-=damage;
        if (hp_ < 0) { hp_ = 0; }
        }
    protected:
    float vx_, vy_;
    float hp_;
    float max_hp_;
    };
