
#define MAX_SUBMODELS 12
#define MAX_BLOCKS 64

enum
    {
    MOD_LEFTTOP,
    MOD_LEFTDOWN,
    MOD_RIGHTTOP,
    MOD_RIGHTDOWN,
    MOD_ORIGIN
    };
enum
    {
    POS_PIXELS,
    POS_PART // from 0 to 1
    };

class Block;

class Model
{
public:
Model(Game& gm, Model* model);
virtual void RotateAbs(float dangle);
virtual void RotateRel(float dangle);
virtual void Mirror();
virtual float GetWidth();
virtual float GetHeight();
virtual float GetOrigX();
virtual float GetOrigY();
float GetX();
float GetY();
virtual float GetAngle();
virtual void Draw(sf::RenderWindow& window);
virtual void Update(float dx, float dy);
virtual void UpdatePoint();
void SetSubmodel(Model* model);
void SetMainSubmodel();
~Model();
protected:
float mirror;
Game& gm_;
float x,y;
Model* models[MAX_SUBMODELS];
int submod;
Model* mainblock;
};
float Model::GetAngle() { return mainblock->GetAngle(); }
float Model::GetWidth() { return mainblock->GetWidth(); }
float Model::GetHeight() { return mainblock->GetHeight(); }
float Model::GetOrigX() { return mainblock->GetOrigX(); }
float Model::GetOrigY() { return mainblock->GetOrigY(); }
float Model::GetX() { return x; }
float Model::GetY() { return y; }
void Model::RotateAbs(float dangle)
    {
    mainblock->RotateAbs(dangle);
    }
void Model::RotateRel(float dangle)
    {
    mainblock->RotateRel(dangle);
    }

void Model::SetSubmodel(Model* model)
    {
    if (submod >= MAX_SUBMODELS || submod < 0) { std::cout << "invalid submod value: " << submod << "\n"; return; }
    models[submod] = model;
    ++submod;
    }
void Model::SetMainSubmodel()
    {
    SetSubmodel(mainblock);
    }

void Model::Draw(sf::RenderWindow& window)
    {
    for (int i = 0; i < submod; i++)
        {
        models[i]->Draw(window);
        }
    }
void Model::Update(float dx, float dy)
    {
    mainblock->Update(dx, dy);
    for (int i = 0; i < submod; i++)
        {
        if (models[i] != mainblock)
        models[i]->Update(dx, dy);
        }
    }

void Model::Mirror()
    {
    mirror = -mirror;
    mainblock->Mirror();
    for (int i = 0; i < submod; i++)
        {
        if (models[i] != mainblock)
        models[i]->Mirror();
        }
    }

void Model::UpdatePoint()
    {
    mainblock->UpdatePoint();
    }

Model::Model(Game& gm, Model* model):gm_(gm)
    {
    mirror = 1;
    x = 0;
    y = 0;
    submod = 0;
    mainblock = model;
    }
Model::~Model()
    {
    ;
    }

class Block: public Model
{
public:
void SetSprite (sf::Texture& im, float x, float y, int mod = POS_PART, float rot = 0)
    {
    float orig_x = 0, orig_y = 0;
    image.setTexture(im);
    angle = rot;
    im_width = im.getSize().x;
    im_height = im.getSize().y;
    image.setTextureRect(sf::Rect<int>(0,0,im_width,im_height));
    if (mod == POS_PART)
        {
        orig_x = x*im_width;
        orig_y = y*im_height;
        }
    else
        {
        orig_x = x;
        orig_y = y;
        }
    image.setOrigin(sf::Vector2f(orig_x, orig_y));
    }
Block(Game& gm):Block(gm, NULL)
    {
    ;
    }
Block(Game& gm, Model* model):Model(gm, model)
    {
    SetSprite(gm_.GetTexture("notfound.png"), 0.5, 0.5);
    pmodel = NULL;
    hardp = false;
    im_width = 0;
    im_height = 0;
    point_relx = 0;
    point_rely = 0;
    angle = 0;
    relradius = 0;
    relangle = 0;
    }
float GetWidth() { return im_width; }
float GetHeight() { return im_height; }
float GetAngle() { if(!hardp) return angle; else { return angle+pmodel->GetAngle(); } }
void Mirror()
    {
    mirror = -mirror;
    angle = -angle;
    image.scale(-1.0f,1.0f);
    point_relx = -point_relx;
    if (pmodel)
    UpdatePoint();
    }
void SetPoint (Model* model, float kx, float ky, bool hardpoint = false, int posmod = MOD_ORIGIN, int mod = POS_PART)
    {
    hardp = hardpoint;
    pmodel = model;
    if (mod == POS_PART) { kx*=model->GetWidth(); ky*=model->GetHeight(); }

    switch (posmod) // recalc relaive origin point
        {
        case MOD_ORIGIN: { break; }
        case MOD_LEFTTOP:   { kx = kx-model->GetOrigX();                   ky = ky-model->GetOrigY(); }
        case MOD_LEFTDOWN:  { kx = kx-model->GetOrigX();                   ky = model->GetHeight()-model->GetOrigY()-ky; }
        case MOD_RIGHTDOWN: { kx = model->GetWidth()-model->GetOrigX()-kx; ky = model->GetHeight()-model->GetOrigY()-ky; }
        case MOD_RIGHTTOP:  { kx = model->GetWidth()-model->GetOrigX()-kx; ky = ky-model->GetOrigY(); }
        default: { std::cout << "invalid spoint argument in Block::GetCoords function\n"; break; }
        };
    point_relx = kx/model->GetWidth();
    point_rely = ky/model->GetHeight();
    relangle = atan2(ky,kx); // atan2 (Y first, X second)!
    relradius = sqrt(kx*kx+ky*ky);

    std::cout << "relangle = " << relangle << "\n";
    std::cout << "relradius = " << relradius << "\n";
    }
void UpdatePoint()
    {
    if (pmodel == NULL)
        {
        std::cout << "UpdatePoint() called incorrectrly\n";
        return;
        }
    float kx = point_relx*pmodel->GetWidth();
    float ky = point_rely*pmodel->GetHeight();
    relangle = atan2(ky,kx); // atan2 (Y first, X second)!
    relradius = sqrt(kx*kx+ky*ky);
    }
void Update(float dx, float dy)
    {
    float pangle = 0;
    if (pmodel != NULL) { dx = pmodel->GetX(); dy = pmodel->GetY(); pangle = pmodel->GetAngle(); }

    x = dx+relradius*cos(relangle+pangle);
    y = dy+relradius*sin(relangle+pangle);
    image.setPosition(x, y);
    //std::cout << "angle:" << angle << " pangle:" << pangle << " obj:" << this << "\n";
    //std::cout << "Draw Block("<< image.getPosition().x <<","<<image.getPosition().y<<")!\n" << this;
    if (!hardp)
        image.setRotation(angle*180/M_PI);
    else
        image.setRotation((angle+pangle)*180/M_PI);
    }
void Draw(sf::RenderWindow& window)
    {
    window.draw(image);
    }
void RotateAbs(float dangle)
    {
    angle = dangle*mirror;
    }
void RotateRel(float dangle)
    {
    angle += dangle*mirror;
    }
~Block()
    {
    ;
    }
private:
sf::Sprite image;
float angle;
float relradius; // anchor point radius
float relangle; // anchor point angle
float point_relx, point_rely; // anchor point relative coordinates
float im_width, im_height;
bool hardp;
Model* pmodel;
};

class GameModel
    {
    public:
    GameModel(Game& gm);
    ~GameModel();
    void AddBlock(Model* block);
    void Draw(sf::RenderWindow& window);
    void Update(float x, float y);
    void Mirror();
    private:
    Game& gm_;
    float anim_var;
    Model* mainmodel;
    Model* blocks[MAX_BLOCKS];
    int blockslen;
    };
void GameModel::Mirror()
    {
    mainmodel->Mirror();
    }

void GameModel::Draw(sf::RenderWindow& window)
    {
    mainmodel->Draw(window);
    }
void GameModel::Update(float x, float y)
    {
    anim_var+=0.1f;
    blocks[4]->RotateAbs(sin(anim_var));
    blocks[5]->RotateAbs(sin(anim_var+M_PI));
    blocks[6]->RotateAbs(sin(anim_var)+M_PI_4);
    blocks[7]->RotateAbs(sin(anim_var+M_PI)+M_PI_4);
    mainmodel->Update(x, y);
    }

GameModel::GameModel(Game& gm):gm_(gm)
    {
    anim_var = 0.0f;
    blockslen = 0;
    Block* stblock = new Block(gm_);
    stblock->SetSprite(gm_.GetTexture("bodyfront.png"), 0.5f, 0.5f);

    Block* headblock = new Block(gm_);
    headblock->SetSprite(gm_.GetTexture("headfront.png"), 0.5f, 0.5f);
    headblock->SetPoint(stblock, 0.0f, -0.5f);

    Block* llegblock = new Block(gm_);
    llegblock->SetSprite(gm_.GetTexture("legup.png"), 0.5f, 0.1f);
    llegblock->SetPoint(stblock, -0.1f, 0.45f, false);
    Block* llegdown = new Block(gm_);
    llegdown->SetSprite(gm_.GetTexture("legdown.png"), 0.25f, 0.1f);
    llegdown->SetPoint(llegblock, 0.0f, 0.8f, true);

    Block* rlegblock = new Block(gm_);
    rlegblock->SetSprite(gm_.GetTexture("legup.png"), 0.5f, 0.1f);
    rlegblock->SetPoint(stblock, 0.1f, 0.45f, false);
    Block* rlegdown = new Block(gm_);
    rlegdown->SetSprite(gm_.GetTexture("legdown.png"), 0.25f, 0.1f);
    rlegdown->SetPoint(rlegblock, 0.0f, 0.8f, true);


    Block* larmblock = new Block(gm_);
    larmblock->SetSprite(gm_.GetTexture("lefthand.png"), 0.75f, 0.09f);
    larmblock->SetPoint(stblock, -0.4f, -0.3f);

    Block* rarmblock = new Block(gm_);
    rarmblock->SetSprite(gm_.GetTexture("righthand.png"), 0.25f, 0.09f);
    rarmblock->SetPoint(stblock, 0.4f, -0.3f);

    Model* lleg = new Model(gm_, llegblock);
    lleg->SetMainSubmodel();
    lleg->SetSubmodel(llegdown);
    Model* rleg = new Model(gm_, rlegblock);
    rleg->SetMainSubmodel();
    rleg->SetSubmodel(rlegdown);

    mainmodel = new Model(gm_, stblock);
    mainmodel->SetMainSubmodel();
    mainmodel->SetSubmodel(headblock);
    mainmodel->SetSubmodel(lleg);
    mainmodel->SetSubmodel(rleg);
    mainmodel->SetSubmodel(larmblock);
    mainmodel->SetSubmodel(rarmblock);
    AddBlock(mainmodel);
    AddBlock(stblock);
    AddBlock(lleg);
    AddBlock(rleg);
    AddBlock(llegblock);
    AddBlock(rlegblock);
    AddBlock(llegdown);
    AddBlock(rlegdown);
    AddBlock(larmblock);
    AddBlock(rarmblock);
    }
void GameModel::AddBlock(Model* block)
    {
    if (blockslen >= MAX_BLOCKS) { std::cout << "Too many blocks in game model\n"; return; }
    blocks[blockslen] = block;
    ++blockslen;
    }
GameModel::~GameModel()
    {
    for (int i = 0; i < blockslen; i++)
        {
        delete blocks[i];
        }
    }
