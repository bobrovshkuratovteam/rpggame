
class Creep:public GamePers {
public:
Creep(Game& gm): GamePers(gm)
    {
    sf::Texture& standtex = gm_.GetTexture("creep1.png");
    sf::Texture& fightdtex = gm_.GetTexture("creep2.png");
    sf::Texture& healtex = gm_.GetTexture("creephealth.png");
    Stand.setTexture(standtex);
    Fight.setTexture(fightdtex);
    Health.setTexture(healtex);
    texS2 = sf::Vector2f(standtex.getSize().x/2, standtex.getSize().y/2);
    texhpS2 = sf::Vector2f(healtex.getSize().x/2, healtex.getSize().y/2);
    Health.setOrigin(texhpS2);
    Stand.setOrigin(texS2);
    Fight.setOrigin(texS2);
    UpdHealthPos();

    vx_ = 0;
    vy_ = 0;
    ID=-1; hp_=10; max_hp_=10; x_=300; y_=300; dmg=3; att=10; ms=100;
    drop_ID=-1; attack_cd=-1; animation_cd=-1; Sprite_pos=1;
    issue=4;
    }
int ID; //type of the creep
int dmg, att; //base damage and attack time
int ms; //base movespeed (pixel/sec)
int drop_ID; //ID of "drop function"
int issue; //current issue: 1 - walk, 2 - attack, 3 - sleep, 4 - defend
sf::Sprite Stand; //Stand walk and first fight animation
sf::Sprite Fight; //Second fight animation
sf::Sprite Health;
sf::Sprite Dead; //Corpse animation
sf::Vector2f texS2;
sf::Vector2f texhpS2;
int Sprite_pos; //1 - S, 2- F, 3 - D

int attack_cd;
int animation_cd;

void UpdHealthPos()
    {
    float deltahp = (float)hp_/max_hp_;
    Health.setPosition (x_, y_-texS2.y-texhpS2.y);
    Health.setTextureRect(sf::Rect<int>(0,0,texhpS2.x*2*deltahp, texhpS2.y*2));
    }

void Update(void)
    {
    //std::cout << "issue = " << issue << std::endl;
    x_ += vx_;
    y_ += vy_;
    float distance = gm_.GetDistanceToPlayer(x_,y_);
    if (distance < 200 && distance > 20)
        {
        sf::Vector2f playerpos = gm_.GetPlayerPos();
        vx_ = 2*(playerpos.x-x_)/distance;
        vy_ = 2*(playerpos.y-y_)/distance;
        issue=2;
        }
    else
        {
        issue=4;
        vx_ = 0;
        vy_ = 0;
        }
    if (distance <= 20) {if (attack_cd == -1) attack_cd = 0;}
    else  { attack_cd = -1; }
    if (attack_cd==0) {Sprite_pos=2; animation_cd=0;}
    if (attack_cd!=-1) {if (attack_cd<=att) {attack_cd++;} else {gm_.KickPlayer(dmg); attack_cd=-1;}}
    if (animation_cd!=-1) {if (animation_cd<(int)(att/3)) {animation_cd++;} else {animation_cd=-1; Sprite_pos=1;}}
    UpdHealthPos();
    }
float GetYPos()
    {
    return y_+texS2.y*2;
    }
float GetXPos()
    {
    return x_;
    }
void Draw(sf::RenderWindow& window) {
/*no screen coordinates calculate!*/
switch(Sprite_pos) {
case 1: Stand.setPosition(x_, y_); window.draw(Stand); break;
case 2: Fight.setPosition(x_, y_); window.draw(Fight); break;
case 3: Dead.setPosition(x_, y_); window.draw(Dead); break;
}
window.draw (Health);
}

void Attack(void) {
if (attack_cd==-1) attack_cd=0;
}
//other functions
};
