
enum
    {
    P_FRONT,
    P_BACK,
    P_LEFT,
    P_RIGHT
    };


class Player
{
public:
    Player(Game& gm, int cx, int cy)
        {
        const sf::Texture& headftex = gm.GetTexture("headfront.png");
        const sf::Texture& headrtex = gm.GetTexture("headright.png");
        const sf::Texture& headbtex = gm.GetTexture("headback.png");
        const sf::Texture& bodyftex = gm.GetTexture("bodyfront.png");
        const sf::Texture& bodybtex = gm.GetTexture("bodyback.png");
        const sf::Texture& larmtex = gm.GetTexture("lefthand.png");
        const sf::Texture& rarmtex = gm.GetTexture("righthand.png");
        const sf::Texture& llegtex = gm.GetTexture("leftleg.png");
        const sf::Texture& rlegtex = gm.GetTexture("rightleg.png");
        const sf::Texture& bodyrtex = gm.GetTexture("bodyright.png");

        direction_ = P_FRONT;

        headfront_.setTexture(headftex);
        headback_.setTexture(headbtex);

        headright_.setTexture(headrtex);
        headleft_ = headright_;
        headleft_.scale(-1.f,1.f);

        bodyright_.setTexture(bodyrtex);
        bodyleft_ = bodyright_;
        bodyleft_.scale(-1.f,1.f);

        bodyfront_.setTexture(bodyftex);
        bodyback_.setTexture(bodybtex);


        head_ = &headfront_;
        body_ = &bodyfront_;

        larm_.setTexture(larmtex); rarm_.setTexture(rarmtex);
        lleg_.setTexture(llegtex); rleg_.setTexture(rlegtex);
        cx_ = cx;
        cy_ = cy;
        vx_ = 0;
        vy_ = 0;
        hp_ = 100;
        hp_max_ = 100;

        direction_ = P_FRONT;
        headS2 = sf::Vector2u (headftex.getSize().x/2, headftex.getSize().y/2);
        bodyS2 = sf::Vector2u (bodyftex.getSize().x/2, bodyftex.getSize().y/2);
        llegS2 = sf::Vector2u (llegtex.getSize().x/2, llegtex.getSize().y/2);
        rlegS2 = sf::Vector2u (rlegtex.getSize().x/2, rlegtex.getSize().y/2);
        larmS2 = sf::Vector2u (larmtex.getSize().x/2, larmtex.getSize().y/2);
        rarmS2 = sf::Vector2u (rarmtex.getSize().x/2, rarmtex.getSize().y/2);

        larm_.setOrigin(larmS2.x, larmS2.x); // not a bug
        rarm_.setOrigin(rarmS2.x, rarmS2.x); // not a bug

        lleg_.setOrigin(llegS2.x, llegS2.x); // not a bug
        rleg_.setOrigin(rlegS2.x, rlegS2.x); // not a bug


        headback_.setOrigin(headS2.x, headS2.y);
        headright_.setOrigin(headS2.x, headS2.y);
        headfront_.setOrigin(headS2.x, headS2.y);
        headleft_.setOrigin(headS2.x, headS2.y);

        bodyback_.setOrigin(bodyS2.x, bodyS2.y);
        bodyfront_.setOrigin(bodyS2.x, bodyS2.y);
        bodyright_.setOrigin(bodyS2.x, bodyS2.y);
        bodyleft_.setOrigin(bodyS2.x, bodyS2.y);
        SetOnPosition(cx, cy);
        }
    //Player() { ; }
    void SetOnPositionDown()
        {

        head_->setPosition(cx_, cy_ - bodyS2.y - headS2.y);
        body_->setPosition(cx_, cy_);

        lleg_.setPosition(cx_ - llegS2.x, cy_ + bodyS2.y + llegS2.x);
        rleg_.setPosition(cx_ + rlegS2.x, cy_ + bodyS2.y + rlegS2.x);

        larm_.setPosition(cx_ - bodyS2.x, cy_ - bodyS2.y);
        rarm_.setPosition(cx_ + bodyS2.x, cy_ - bodyS2.y);
        }
    void SetOnPositionUp()
        {
        SetOnPositionDown();
        }
    void SetOnPositionRight()
        {
        SetOnPositionDown();
        }
    void SetOnPositionLeft()
        {
        SetOnPositionDown();
        }
    void SetOnPosition(float cx, float cy)
        {

        cx_ = cx;
        cy_ = cy;
        switch(direction_)
            {
            case P_FRONT:
                SetOnPositionDown();
                break;
            case P_BACK:
                SetOnPositionUp();
                break;
            case P_LEFT:
                SetOnPositionLeft();
                break;
            case P_RIGHT:
                SetOnPositionRight();
                break;
            default:
                std::cout << "Undefined player direction\n";
            };
        }
    void MoveBy(float dx, float dy)
        {
        SetOnPosition(cx_+dx, cy_+dy);
        }
    void Update()
        {
        SetOnPosition(cx_+vx_, cy_+vy_);
        }
    void Draw(sf::RenderWindow& window)
        {
        //std::cout << "draw man x:" << head_.getPosition().x << "; y:" << head_.getPosition().y << std::endl;

        window.draw(*head_);
        window.draw(*body_);
        window.draw(larm_);
        window.draw(rarm_);
        window.draw(lleg_);
        window.draw(rleg_);
        }
    void GoUp()
        {
        direction_ = P_BACK;
        vy_ = -v_;
        head_ = &headback_;
        body_ = &bodyback_;
        }
    void GoDown()
        {
        direction_ = P_FRONT;
        vy_ = v_;
        head_ = &headfront_;
        body_ = &bodyfront_;
        }
    void GoLeft()
        {
        direction_ = P_LEFT;
        vx_ = -v_;
        head_ = &headleft_;
        body_ = &bodyleft_;
        }
    void GoRight()
        {
        direction_ = P_RIGHT;
        vx_ = v_;
        head_ = &headright_;
        body_ = &bodyright_;
        }
    void StopUp()
        {
        if(vy_ < 0)
        vy_ = 0;
        }
    void StopDown()
        {
        if(vy_ > 0)
        vy_ = 0;
        }
    void StopLeft()
        {
        if(vx_ < 0)
        vx_ = 0;
        }
    void StopRight()
        {
        if (vx_ > 0)
        vx_ = 0;
        }
    ~Player()
        {
        ;
        }
    float GetXPos()
        {
        return cx_;
        }
    float GetYPos()
        {
        return cy_+bodyS2.y+llegS2.y*2;
        }
    float GetY()
        {
        return cy_;
        }
    void Kick(int damage)
        {
        hp_ -= damage;
        std::cout<<hp_<<"/"<<hp_max_<<" hp\n";
        }
    sf::Vector2f GetPos()
        {
        return sf::Vector2f(cx_, cy_);
        }
    float GetDist(float ax, float ay)
        {
        return sqrt((cx_-ax)*(cx_-ax)+(cy_-ay)*(cy_-ay));
        }
    int GetHP()
        {
        return hp_;
        }
    float GetRelativeHP()
        {
        return (float)hp_/hp_max_;
        }
private:
sf::Sprite* head_;
sf::Sprite headfront_;
sf::Sprite headback_;
sf::Sprite headright_;
sf::Sprite headleft_;

sf::Sprite* body_;
sf::Sprite bodyfront_;
sf::Sprite bodyback_;
sf::Sprite bodyright_;
sf::Sprite bodyleft_;

sf::Sprite larm_;
sf::Sprite rarm_;
sf::Sprite lleg_;
sf::Sprite rleg_;
sf::Vector2u bodyS2; //  half size
sf::Vector2u headS2;
sf::Vector2u larmS2;
sf::Vector2u rarmS2;
sf::Vector2u llegS2;
sf::Vector2u rlegS2;
float cx_;
float cy_;

float vx_;
float vy_;
int hp_;
int hp_max_;
static float v_;

int direction_;
};
