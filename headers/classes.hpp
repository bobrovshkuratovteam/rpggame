
class Player;
class Creep;
class Model;
class Block;
class GameModel;
class Game
{
public:
sf::Texture* textures_;
    Game(int scrw, int scrh);
    ~Game();
    int Update();
    int Draw(sf::RenderWindow& window);
    float GetDistanceToPlayer(float ax, float ay);
    float GetDistanceToCreep();
    sf::Vector2f GetVectorToCreep();
    sf::Vector2f GetPlayerPos();
    sf::Texture& GetTexture(char* filename);
    int KickPlayer(int damage); // 0-100
    int Drop (float x, float y, int itemid);
    void MouseClick (int x, int y);
    void KeyPress(int code);
    void KeyRelease(int code);
private:
sf::Sprite healthbar_;
sf::Sprite healthfull_;
float healthwidth_;
float healthheight_;
int scrw_;
int scrh_;
Player* player_;
Creep* creep_;
GameModel* model;
};
